﻿using System;
using System.Collections.Generic;

namespace prep.utility.filtering
{
	public static class EnumerableFilteringExtensions
	{
		public static EnumerableMatchingExtensionPoint<ItemToMatch, PropertyType> where<ItemToMatch, PropertyType>(this IEnumerable<ItemToMatch> enumerable,
		                                                                                                           PropertyAccessor<ItemToMatch, PropertyType> accessor)
		{
			return new EnumerableMatchingExtensionPoint<ItemToMatch, PropertyType>(enumerable, accessor);
		}

		public static IEnumerable<ItemToMatch> greater_than<ItemToMatch, PropertyType>(this EnumerableMatchingExtensionPoint<ItemToMatch, PropertyType> extension_point,
		                                                                               PropertyType value) where PropertyType : IComparable<PropertyType>
		{
			IMatchAn<ItemToMatch> match = Where<ItemToMatch>.has_a(extension_point.accessor).greater_than(value);
			return extension_point.items_to_filter.all_items_matching(match);
		}

		public static IEnumerable<ItemToMatch> greater_than<ItemToMatch>(this EnumerableMatchingExtensionPoint<ItemToMatch, DateTime> extension_point,
		                                                                               int value)
		{
			IMatchAn<ItemToMatch> match = Where<ItemToMatch>.has_a(extension_point.accessor).greater_than(new DateTime(value, 1, 1));
			return extension_point.items_to_filter.all_items_matching(match);
		}

	}
}