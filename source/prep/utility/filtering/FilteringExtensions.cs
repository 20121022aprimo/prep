﻿using System;
using prep.utility.ranges;

namespace prep.utility.filtering
{
  public static class FilteringExtensions
  {
    public static IMatchAn<ItemToMatch> equal_to<ItemToMatch, PropertyType>(
      this IProvideAccessToCreatingFilters<ItemToMatch, PropertyType> extension_point, PropertyType value)
    {
      return equal_to_any(extension_point, value);
    }

    public static IMatchAn<ItemToMatch> equal_to_any<ItemToMatch, PropertyType>(
      this IProvideAccessToCreatingFilters<ItemToMatch, PropertyType> extension_point, params PropertyType[] values)
    {
      return create_using(extension_point, new EqualToAny<PropertyType>(values));
    }

    public static IMatchAn<ItemToMatch> create_using<ItemToMatch, PropertyType>(
      this IProvideAccessToCreatingFilters<ItemToMatch, PropertyType> extension_point, IMatchAn<PropertyType> condition)
    {
      return extension_point.create_specification_using(condition);
    }

    public static IMatchAn<ItemToMatch> falls_in<ItemToMatch, PropertyType>(
      this IProvideAccessToCreatingFilters<ItemToMatch, PropertyType> extension_point, Range<PropertyType> range ) where PropertyType : IComparable<PropertyType>
    {
      throw new NotImplementedException();
    }
    public static IMatchAn<ItemToMatch> between<ItemToMatch, PropertyType>(
      this IProvideAccessToCreatingFilters<ItemToMatch, PropertyType> extension_point, PropertyType start, PropertyType end) where PropertyType : IComparable<PropertyType>
    {
      return create_using(extension_point, new IsBetween<PropertyType>(start, end));
    }

    public static IMatchAn<ItemToMatch> greater_than<ItemToMatch, PropertyType>(
      this IProvideAccessToCreatingFilters<ItemToMatch, PropertyType> extension_point, PropertyType value) where PropertyType : IComparable<PropertyType>
    {
      return create_using(extension_point, new IsGreaterThan<PropertyType>(value));
    }
  }
}
