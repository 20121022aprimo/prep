namespace prep.utility.filtering
{
  public delegate ReturnTypeOfPropertyToPointAt PropertyAccessor
    <in ItemToPullValueFrom, out ReturnTypeOfPropertyToPointAt>(ItemToPullValueFrom item);
}