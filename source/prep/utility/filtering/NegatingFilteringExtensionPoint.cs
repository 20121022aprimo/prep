﻿namespace prep.utility.filtering
{
  public class NegatingFilteringExtensionPoint<ItemToFilter,PropertyType> : IProvideAccessToCreatingFilters<ItemToFilter,PropertyType>
  {
    IProvideAccessToCreatingFilters<ItemToFilter, PropertyType> original;

    public NegatingFilteringExtensionPoint(IProvideAccessToCreatingFilters<ItemToFilter, PropertyType> original)
    {
      this.original = original;
    }

    public IMatchAn<ItemToFilter> create_specification_using(IMatchAn<PropertyType> condition)
    {
      return original.create_using(condition).not();
    }
  }
}