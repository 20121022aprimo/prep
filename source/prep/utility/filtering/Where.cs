namespace prep.utility.filtering
{
  public class Where<ItemToMatch>
  {
    public static MatchingExtensionPoint<ItemToMatch, PropertyType> has_a<PropertyType>(
      PropertyAccessor<ItemToMatch, PropertyType> accessor)
    {
      return new MatchingExtensionPoint<ItemToMatch, PropertyType>(accessor);
    }
  }
}