﻿using System;

namespace prep.utility.filtering
{
	public static class DateTimeFilteringExtensions
	{
		 public static IMatchAn<ItemToMatch> greater_than<ItemToMatch>(this IProvideAccessToCreatingFilters<ItemToMatch, DateTime> extension_point, int year)
		 {
		   return extension_point.create_specification_using(Where<DateTime>.has_a(x => x.Year).greater_than(year));
		 }
	}
}
