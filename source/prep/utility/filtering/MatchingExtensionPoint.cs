using System.Collections.Generic;

namespace prep.utility.filtering
{
  public class MatchingExtensionPoint<ItemToMatch, PropertyType> : IProvideAccessToCreatingFilters<ItemToMatch,PropertyType>
  {
    PropertyAccessor<ItemToMatch, PropertyType> accessor; 

    public IProvideAccessToCreatingFilters<ItemToMatch, PropertyType> not
    {
      get 
      {
        return new NegatingFilteringExtensionPoint<ItemToMatch, PropertyType>(this);
      }
    }

    public MatchingExtensionPoint(PropertyAccessor<ItemToMatch, PropertyType> accessor)
    {
      this.accessor = accessor;
    }

    public IMatchAn<ItemToMatch> create_specification_using(IMatchAn<PropertyType> condition)
    {
      return new PropertyMatch<ItemToMatch, PropertyType>(accessor, condition);
    }
  }

  public class EnumerableMatchingExtensionPoint<ItemToMatch, PropertyType>
  {
    public PropertyAccessor<ItemToMatch, PropertyType> accessor { get; private set; }
    public IEnumerable<ItemToMatch> items_to_filter { private set; get; }

    public EnumerableMatchingExtensionPoint(IEnumerable<ItemToMatch> enumerable, PropertyAccessor<ItemToMatch, PropertyType> accessor)
    {
      this.accessor = accessor;
      this.items_to_filter = enumerable;
    }
  }
}