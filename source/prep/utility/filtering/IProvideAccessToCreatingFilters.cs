namespace prep.utility.filtering
{
  public interface IProvideAccessToCreatingFilters<in ItemToMatch, out PropertyType>
  {
    IMatchAn<ItemToMatch> create_specification_using(IMatchAn<PropertyType> condition);
  }
}